# Change Log

## [Version 1.0.2](https://gitlab.com/yanbreu/myip/tree/v1.0.2)
- Responsive Styling

## [Version 1.0.1](https://gitlab.com/yanbreu/myip/tree/v1.0.2)
- Fix Assets in Docker image

## [Version 1.0.0 - Initial Release](https://gitlab.com/yanbreu/myip/tree/v1.0.2)
- Show IP Address
- Show Hostname
- Docker image