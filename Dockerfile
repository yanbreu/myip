# This file is a template, and might need editing before it works on your project.
FROM golang:alpine AS builder

WORKDIR /go/src/gitlab.com/yanbreu/myip

COPY . /go/src/gitlab.com/yanbreu/myip

RUN CGO_ENABLED=0 GOOS=linux go build -v -a -installsuffix cgo -o myip-bin .

FROM alpine:latest

WORKDIR /data/app
COPY assets assets
COPY --from=builder /go/src/gitlab.com/yanbreu/myip/myip-bin myip
CMD ["./myip"]
