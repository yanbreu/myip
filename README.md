# MyIP Address
 
This little web service will show you your ip and hostname. It's witten in go.

## Api


Get your current IP & hostname:
```
http://localhost:8081/
``` 

Get your unformatted current IP:
```
http://localhost:8081/ip
``` 

Get your unformatted current hostname:
```
http://localhost:8081/hostname
```