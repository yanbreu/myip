package myip

import (
	"net"
	"net/http"
	"strings"
)

func getIPAdress(r *http.Request) (string, error) {
	for _, h := range []string{"X-Forwarded-For", "X-Real-Ip"} {
		addresses := strings.Split(r.Header.Get(h), ",")
		// march from right to left until we get a public address
		// that will be the address right before our proxy.
		for i := 0; i < len(addresses); i++ {
			ip := strings.TrimSpace(addresses[i])
			// header can contain spaces too, strip those out.
			realIP := net.ParseIP(ip)
			if !realIP.IsGlobalUnicast() {
				// bad address, go to next
				continue
			}
			return ip, nil
		}
	}

	ip, err := removePortFromIP(r.RemoteAddr)
	return ip, err
}

func removePortFromIP(ip string) (string, error) {
	ip, _, err := net.SplitHostPort(ip)
	if err != nil {
		return "", err
	}
	return ip, nil
}

func getHostname(ip string) string {
	names, err := net.LookupAddr(ip)
	if err != nil {
		return ip
	}

	if len(names) > 0 {
		hostname := names[0]

		if hostname[len(hostname)-1] == '.' {
			hostname = hostname[:len(hostname)-1]
		}
		return hostname
	}

	return ip
}
