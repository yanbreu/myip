package myip

import (
	"io"
	"log"
	"net/http"
	"text/template"
)

const doc = `
<!DOCTYPE html>
<html>
	<head>
		<style type="text/css">
		.info {
			margin: 0 auto;
			max-width: 600px;
			background: #e1e1e1;
			border: 1px solid;
			padding: 10px;
			line-height: 1.4rem;
			font-size: 16px;
			margin-top: 70px;
			font-family: monospace;
		}
		.links {
			line-height: 1em;
			margin-top: 20px;
		}
		.links a {
			font-size: 10px;
			color: #0043ff;
			text-decoration: none;
		}
		</style>
		<title>Your IP is {{.IP}}</title>
		<link rel="apple-touch-icon" sizes="57x57" href="/assets/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/assets/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/assets/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/assets/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/assets/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/assets/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/assets/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/assets/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/assets/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="/assets/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="/assets/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon-16x16.png">
		<link rel="manifest" href="/assets/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/assets/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<link rel="shortcut icon" href="/assets/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/assets/favicon.ico" type="image/x-icon">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    </head>
	<body>
		<div class="info">
			<div><strong>IP Address:</strong></div>
			<div>{{.IP}}</div>
			<div><strong>Hostname:</strong></div>
			<div>{{.Hostname}}</div>
			{{if ne .Proxy ""}}
			<br/>
			<div><small>Currently it looks like you are using a proxy. The proxy IP is <strong>{{.Proxy}}</strong>.</small></div>
			{{end}}
			<div class="links">
				<a href="/ip">IP Address only</a>
				<a href="/hostname">Hostname only</a>
			</div>
		</div>
    </body>
</html>
`

// IPInfo contains the data for the template
type IPInfo struct {
	IP       string
	Hostname string
	Proxy    string
}

func index(w http.ResponseWriter, r *http.Request) {
	ip, err := getIPAdress(r)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	proxy := ""
	remoteIP, _ := removePortFromIP(r.RemoteAddr)
	if remoteIP != "" && remoteIP != ip {
		proxy = remoteIP
	}

	hostname := getHostname(ip)

	w.Header().Add("Content Type", "text/html")

	// The template name "template" does not matter here
	templates := template.New("template")
	// "doc" is the constant that holds the HTML content
	templates.New("doc").Parse(doc)
	info := IPInfo{IP: ip, Hostname: hostname, Proxy: proxy}

	templates.Lookup("doc").Execute(w, info)

}

func ip(w http.ResponseWriter, r *http.Request) {
	ip, err := getIPAdress(r)
	if err != nil {
		log.Printf("%s", err.Error())
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	log.Printf("%s", ip)
	io.WriteString(w, ip)
}

func hostname(w http.ResponseWriter, r *http.Request) {
	ip, err := getIPAdress(r)
	if err != nil {

		log.Printf("%s", err.Error())
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	hostname := getHostname(ip)

	log.Printf("%s", hostname)
	io.WriteString(w, hostname)

	return
}

func faviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "assets/favicon.ico")
}

// ListenAndServe runs the webserver
func ListenAndServe(addr string, hander http.Handler) {
	http.HandleFunc("/", index)
	http.HandleFunc("/ip", ip)
	http.HandleFunc("/hostname", hostname)
	fs := http.FileServer(http.Dir("assets"))
	http.Handle("/assets/", http.StripPrefix("/assets/", fs))
	http.HandleFunc("/favicon.ico", faviconHandler)

	log.Printf("Start server on %s", addr)
	http.ListenAndServe(addr, hander)
}
